package JumpPointSearch;
import java.util.*;
import java.util.concurrent.TimeUnit;

/*

	[0,0] is top left corner
	[0,1] is moving down one
	[1,0] is moving to the right one
	[w-1, h-1] is the bottom right corner
	
	start point color 8bc34a
	goal point color f44336


	When meet an already visited interestring jump node
	I assume no further search will be needed on that direction

*/
public class JumpPointMain{
	final int SLEEP_TIME = 100; // milliseconds
	static boolean animation_mode = true;
	
	ImageMap im;
	PriorityHeap<TreeNode> open_tree_node;
	//TreeNode jp_root;
	//TreeNode cur_tree_node;


	/*


	heristic search h = distance so far + distance to goal



	*/
	public void search(){
		/*
		jp_root = new TreeNode(new Point(im.start), new Point(), null);
		open_tree_node.add(jp_root);
		*/
		addJump(im.start, Point.zero, null);
		
		while (open_tree_node.size() > 0) {
			TreeNode cur_tree_node = open_tree_node.remove();
			System.out.println("poping "+cur_tree_node+"\n"+open_tree_node);
			
			if(cur_tree_node.p.equals(im.goal)){
				break;
			}
			
			Point dir = cur_tree_node.dir;
			Point cur = cur_tree_node.p;
			
			if (dir.equals(Point.zero)) {
				// it's the root node, search all direction
				search_all(cur_tree_node);
			}else if(dir.getX() == 0 || dir.getY() == 0){
				// meet a force neighbour while in horizontal or vertical search
				Point n1 = new Point();
				Point n2 = new Point();
				if(dir.getX() == 0){
					n1 = new Point(1, 0);
					n2 = new Point(-1, 0);
				}else if(dir.getY() == 0){
					n1 = new Point(0, 1);
					n2 = new Point(0, -1);
				}else{
					System.out.println("error force neighbour coordinate");
				}
				// handle the two force neighbour cases
				if(im.isWall(cur.add(n1))){
					search_diagonal(cur, dir.add(n1), cur_tree_node);
				}
				if(im.isWall(cur.add(n2))){
					search_diagonal(cur, dir.add(n2), cur_tree_node);
				}
			}else if(dir.getX() != 0 && dir.getY() != 0){
				// meet a force neightbour while in diagonal search
				// handle the two force neighbour cases
				if(im.isWall(cur.add(-1*dir.getX(), 0))){
					search_diagonal(cur, new Point(-1*dir.getX(), dir.getY()), cur_tree_node);
				}
				if(im.isWall(cur.add(0, -1*dir.getY()))){
					search_diagonal(cur, new Point(dir.getX(), -1*dir.getY()), cur_tree_node);
				}
			}else{
				System.out.println("sweeping search direction error");
			}

		}
		System.out.println("Finish sweeping..");
		
	}

	public void search_all(TreeNode parent){
		Point cur = parent.p;
		
		Point dir = new Point(-1, 0);
		search_straight(cur, dir, parent);
		dir = new Point(1, 0);
		search_straight(cur, dir, parent);
		dir = new Point(0, 1);
		search_straight(cur, dir, parent);
		dir = new Point(0, -1);
		search_straight(cur, dir, parent);
		
		dir = new Point(1, 1);
		search_diagonal(cur, dir, parent);
		dir = new Point(-1, 1);
		search_diagonal(cur, dir, parent);
		dir = new Point(1, -1);
		search_diagonal(cur, dir, parent);
		dir = new Point(-1, -1);
		search_diagonal(cur, dir, parent);
		
	}

	public void search_diagonal(Point cur, Point dir, TreeNode parent){
		im.change_foot_color();
		Point next = cur.add(dir);
		while(true){
			if(!is_in_map(next) || im.isWall(next)){
				//System.out.println("d out of map");
				break;
			}
			visit(next);
			// found an interesting node which has already been visited
			if(im.jumpPoint[next.getX()][next.getY()] != null){
				addJump(next, dir, parent);
				break;
			}
			if(next.equals(im.goal)){
				//add(next, dir);
				TreeNode t = addJump(next, dir, parent);
				search_diagonal(next, dir, t);
			}
			if(im.isWall(next.add(-1*dir.getX(), 0)) && !im.isWall(next.add(-1*dir.getX(), dir.getY()))){
				TreeNode t = addJump(next, dir, parent);
				search_diagonal(next, dir, t);
			}
			if(im.isWall(next.add(0, -1*dir.getY())) && !im.isWall(next.add(dir.getX(), -1*dir.getY()))){
				TreeNode t = addJump(next, dir, parent);
				search_diagonal(next, dir, t);
			}

			TreeNode dia = new TreeNode(next, dir, parent);
			search_straight(next, new Point(dir.getX(), 0), dia);
			search_straight(next, new Point(0, dir.getY()), dia);
			if (dia.children.size() > 0) {
				parent.children.add(dia);
				im.jumpPoint[next.getX()][next.getY()] = dia;
				showJump(next);
				search_diagonal(next, dir, dia);
			}
			next = next.add(dir);
		}
	}

	public void search_straight(Point cur, Point dir, TreeNode parent){
		Point n1 = new Point();
		Point n2 = new Point();
		if(dir.getX() == 0){
			n1 = new Point(1, 0);
			n2 = new Point(-1, 0);
		}else if(dir.getY() == 0){
			n1 = new Point(0, 1);
			n2 = new Point(0, -1);
		}else{
			System.out.println("error force neighbour coordinate");
		}

		Point next = cur.add(dir);
		while(true){
			
			if(!is_in_map(next) || im.isWall(next)){
				break;
			}
			visit(next);
			if(im.jumpPoint[next.getX()][next.getY()] != null){
				addJump(next, dir, parent);
				break;
			}

			if(next.equals(im.goal)){
				//add(next, dir);

				TreeNode t = addJump(next, dir, parent);
				search_straight(next, dir, t);
			}

			if(!is_in_map(next.add(dir))){
				//System.out.print("out of map\n");
				break;
			}

			// found force neighbours			
			// up
			if(is_in_map(next.add(n1)) && im.isWall(next.add(n1)) 
				&& !im.isWall(next.add(dir).add(n1))){
				//add(next, dir);
				System.out.println("up");
				
				TreeNode t = addJump(next, dir, parent);
				search_straight(next, dir, t);
			}
			// down
			if(is_in_map(next.add(n2)) && im.isWall(next.add(n2)) 
				&& !im.isWall(next.add(dir).add(n2))){
				//add(next, dir);
				System.out.println("down");
				
				TreeNode t = addJump(next, dir, parent);
				search_straight(next, dir, t);
			}

			next = next.add(dir);
		}
	}

	public void showJump(Point p){
		delay();
		if (!p.equals(im.start) && !p.equals(im.goal)) {
			im.jumpnode(p);
		}
	}

	public TreeNode addJump(Point p, Point dir, TreeNode parent){
		TreeNode insteresting = im.jumpPoint[p.getX()][p.getY()];
		//TreeNode node_new = new TreeNode(p, dir, parent);
		if(insteresting != null){
			// node already exist in the tree
			// add parent to the list, maybe the distance to parent
			// changes again after future discovery, when recalculate 
			// children h value, we need all the parent
		
			insteresting.parent.add(parent);
			parent.children.add(insteresting);
			//insteresting.dir = dir;
			// the new path to insteresting jump point may change the 
			// shortest path to this jump point and affects the children
			// of this jump point
			insteresting.recalculate_h();
			open_tree_node.heapfy();
		
		}else{
			insteresting = new TreeNode(p, dir, parent);
			im.jumpPoint[p.getX()][p.getY()] = insteresting;
			if(parent != null){
				parent.children.add(insteresting);
			}
			open_tree_node.add(insteresting);
			System.out.println("add "+p+" \n"+open_tree_node);

		}
		
		delay();
		if (!p.equals(im.start) && !p.equals(im.goal)) {
			im.jumpnode(p);
		}
		return insteresting;
		
	}

	public boolean is_in_map(Point p){
		return p.getX() > -1 && p.getX() < im.w && p.getY() > -1 && p.getY() < im.h;
	}

	public void visit(Point p){
		delay();
		if (!p.equals(im.start) && !p.equals(im.goal) && im.jumpPoint[p.getX()][p.getY()] == null) {
			im.footprint(p);

		}
	}

	/*
	public void print_queue(){
		System.out.print("Open Queue: ");
		for(TreeNode t : open_tree_node){
			System.out.print(""+t.p+" ");
		}
		System.out.println("");
	}
	*/

	public void delay(){
		if(JumpPointMain.animation_mode){
			try{
				TimeUnit.MILLISECONDS.sleep(SLEEP_TIME);
			}catch(InterruptedException ex){
				ex.printStackTrace();
			}
		}
	}


	public void init(String name){
		im = new ImageMap(name);
		TreeNode.goal = im.goal;
		open_tree_node = new PriorityHeap<TreeNode>(new PriorityHeap.TreeNodeComparator());
	}


	public void print_path(){
		System.out.println("Printing path..");
		TreeNode goal = im.jumpPoint[im.goal.getX()][im.goal.getY()];
		if(goal == null){
			System.out.println("null goal node, error!");
			return;
		}

		TreeNode cur = goal, parent = cur.find_shortest_distance_parent();

		while(parent != null){
			System.out.println("print segement "+cur.p+"-"+parent.p);
			int dx = parent.p.getX() - cur.p.getX();
			int dy = parent.p.getY() - cur.p.getY();
			if(Math.abs(dx) == Math.abs(dy)){
				draw_until(cur.p, new Point(Integer.signum(dx), Integer.signum(dy)), parent.p);
			}else if(dx == 0){
				//System.out.println("dy:"+dy+" sign:"+Integer.signum(dy));
				draw_until(cur.p, new Point(0, Integer.signum(dy)), parent.p);
			}else if(dy == 0){
				draw_until(cur.p, new Point(Integer.signum(dx), 0), parent.p);
			}else{
				System.out.println("error while printing path, path finding error");
			}
			cur = parent;
			parent = parent.find_shortest_distance_parent();
		}
		im.finish_set_color();
		System.out.println("finish printing..");
	}

	public void draw_until(Point src, Point dir, Point dest){
		Point temp = src.add(dir);
		while(!temp.equals(dest)){
			//System.out.println("draw "+temp);
			im.set_path_color(temp);
			temp = temp.add(dir);
		}
	}

	public static void main(String[] args) {
		if(args.length != 3){
			System.out.println("Missing input file.");
			print_usage();
			System.exit(2);
		}
		if(args[0].equals("f")){
			JumpPointMain.animation_mode = false;
		}else if(args[0].equals("a")){
			JumpPointMain.animation_mode = true;
		}else{
			System.out.println("Invalid flag ->"+args[0]+"<- can only be 'a' or 's'");
			print_usage();
			System.exit(2);
		}

		//PriorityHeap.test();
		try{
			ImageMap.zoom_level = Integer.parseInt(args[2]);
		}catch(NumberFormatException ex){
			System.out.println("zoom level not a numner");
			print_usage();
			System.exit(2);
		}
		if(ImageMap.zoom_level <= 0){
			System.out.println("zoom level can't be negative or zero");
			print_usage();
			System.exit(2);
		}

		JumpPointMain j = new JumpPointMain();
		j.init(args[1]);
		j.search();
		j.print_path();
		System.out.println("Idling...");
		
	}

	public static void print_usage(){
		System.out.println(
			"----------------------------------\n"+
			"*              Usage             *\n"+
			"----------------------------------\n"+
			"The color for goal is 0XFFF44336\n"+
			"The color for start is 0XFF8BC34A\n\n"+
			"Use this command to run --> java JumpPointSearch.JumpPointMain a input.png 10 <--\n\n"+
			"a means animation mode, you can also change it to f to run without stopping.\n"+
			"10 means the zoom level of the result image, increase it to make the image bigger\n"+
			"There are some default image, try ->test_simple_1.png<- \n"+
			"Or ->test_median_1.png<- Or ->test_hard_1.png<-\n\n\n");
	}
}












