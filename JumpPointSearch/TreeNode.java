package JumpPointSearch;
import java.util.*;


public class TreeNode {
    
    public static Point goal;

    public ArrayList<TreeNode> parent;
    public ArrayList<TreeNode> children;
    public double distance_from_start = 0;
    public double h;

    public Point p;
    public Point dir;
    
    public TreeNode(Point cur, Point dir, TreeNode p) {
        this.parent = new ArrayList<TreeNode>();
        if(p != null){
        	this.parent.add(p);
    	}
        this.p = cur;
        this.dir = dir;
        children = new ArrayList<TreeNode>();
        calculate_h();
    }

    public void recalculate_h(){
    	calculate_h();
    	for(TreeNode t : children){
    		t.calculate_h();
    	}
    }

    private void calculate_h(){
    	if(parent.size() == 0){
    		h = Point.distance(this.p, goal);
    		distance_from_start = 0;
    	}else{
    		TreeNode small_parent = find_shortest_distance_parent();
    		distance_from_start = small_parent.distance_from_start 
    			+ Point.distance(small_parent.p, this.p);
	    	h = distance_from_start + Point.distance(this.p, goal);
    	}
    }

    public TreeNode find_shortest_distance_parent(){
    	if(parent.size() == 0){ return null; }
    	double smallest = parent.get(0).distance_from_start;
		TreeNode small_parent = parent.get(0);
		for(TreeNode t : parent){
			if(t.distance_from_start < smallest){
				smallest = t.distance_from_start;
				small_parent = t;
			}
		}
		return small_parent;

    }


    public String toString(){
    	return this.p + ":" + String.format("%.2f", h);
    }

}



/*

needs current node position
needs parent distance
needs goal node position




*/