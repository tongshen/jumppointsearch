package JumpPointSearch;


import java.awt.Canvas;
import java.awt.Graphics;
import javax.swing.JFrame;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;
import javax.imageio.*;


public class ImageMap extends Canvas{

	final int GOAL_COLOR = 0XFFF44336;
	final int START_COLOR = 0XFF8BC34A;
	final int FOOTPRINT_COLOR_1 = 0XFFCCEBFF;
	final int FOOTPRINT_COLOR_2 = 0XFF66D9FF;
	final int FOOTPRINT_COLOR_3 = 0XFF6666FF;
	final int JUMP_NODE_COLOR = 0XFFFFFF66;
	final int WALL_COLOR = 0xFF000000;
	final int PATH_COLOR = 0XFF33CCCC;

	public static int zoom_level = 8;
	BufferedImage img;
	int w, h;
	Point start, goal; 
	TreeNode[][] jumpPoint;
	int foot_color = FOOTPRINT_COLOR_1;

	public boolean isWall(Point p){
		return img.getRGB(p.getX(), p.getY()) == WALL_COLOR;
	}

	public void jumpnode(Point p){
		img.setRGB(p.getX(), p.getY(), JUMP_NODE_COLOR);
		paint(this.getGraphics());
	}

	public void change_foot_color(){
		/*
		switch(foot_color){
			case FOOTPRINT_COLOR_1:
				foot_color = FOOTPRINT_COLOR_2;
				break;
			case FOOTPRINT_COLOR_2:
				foot_color = FOOTPRINT_COLOR_3;
				break;
			case FOOTPRINT_COLOR_3:
				foot_color = FOOTPRINT_COLOR_1;
				break;
			default:
				foot_color = FOOTPRINT_COLOR_1;
		}
		*/
	}

	public void footprint(Point p){
		img.setRGB(p.getX(), p.getY(), foot_color);
		paint(this.getGraphics());
	}

	public void set_path_color(Point p){
		img.setRGB(p.getX(), p.getY(), PATH_COLOR);
	}

	public void finish_set_color(){
		paint(this.getGraphics());
	}

	public ImageMap(String filename){

		// load image
		String name = "JPS";
		try {
           	File f = new File(filename);
           	name = f.getName();
           	img = ImageIO.read(f);
        } catch (IOException e) {
        	System.out.println("IOException "+e.getMessage());
        	JumpPointMain.print_usage();
        	System.exit(1);
        }

        // processing image
        w = img.getWidth();
        h = img.getHeight();

        System.out.println("Image dimension "+w+"X"+h);
        jumpPoint = new TreeNode[w][h];
/*
		for (int i = 0; i < visited.length; i++) {
			for (int j = 0; j < visited[i].length; j++) {
				System.out.print(visited[i][j] + " ");
			}
			System.out.println("");
		}
*/
        start = new Point(-1, -1);
        goal = new Point(-1, -1);
        for (int i = 0; i < w; i++) {
			for (int j = 0; j < h; j++) {
				int color = img.getRGB(i, j);
				if(color == START_COLOR){
					start = new Point(i, j);
				}else if(color == GOAL_COLOR){
					goal = new Point(i, j);
				}
			}
		}
		if(start.getX() == -1 || goal.getX() == -1){
			System.out.println("Missing goal or start point.");
			JumpPointMain.print_usage();
			System.exit(1);
		}
		System.out.println("Start coordinate: "+start+"\nGoal coordinate: "+goal);

		// rendering image
		JFrame frame = new JFrame(name);
        
        this.setSize(w*zoom_level, h*zoom_level);
        frame.getContentPane().setSize(w*zoom_level, h*zoom_level);
        frame.getContentPane().add(this);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
	}

	public void paint(Graphics g) {
        g.drawImage(img, 0, 0, w*zoom_level, h*zoom_level, 0, 0, w, h, null);
    }
}

