package JumpPointSearch;
import java.util.ArrayList;
import java.util.Comparator;

public class PriorityHeap<T>{

	private ArrayList<T> heap;
	private Comparator c;

	public PriorityHeap(Comparator compare){
		heap = new ArrayList<T>();

		this.c = compare;
	}

	public void add(T e){
		heap.add(e);
		bubble_up(heap.size()-1);

	}

	public T remove(){
		if(heap.size() == 0){ return null; }
		if(heap.size() == 1){ return heap.remove(0); }
		T result = heap.get(0);
		T last = heap.remove(heap.size()-1);
		heap.set(0, last);
		bubble_down(0);
		return result;
	}

	public int size(){ return heap.size(); }

	private int left(int i){
		return 2*i+1;
	}

	private int right(int i){
		return 2*i+2;
	}

	private int parent(int i){
		return (i-1)/2;
	}

	public void heapfy(){
		if(heap.size() < 2){ return; }
		int count = 1;
		while(count*4 - 1 < heap.size()){
			count = count * 2;
		}
		int index = count * 2 - 2;

		while(index > -1){
			bubble_down(index);

			index--;
		}
	}

	private void bubble_down(int i){
		if(i >= heap.size()){
			return;
		}

		int left = this.left(i);
		int right = this.right(i);
		int smallest = i;
		// if left smaller than i
		if(left < heap.size() && c.compare(heap.get(left), heap.get(i)) < 0){
			smallest = left;
		}

		// if right smaller than smallest
		if(right < heap.size() && c.compare(heap.get(right), heap.get(smallest)) < 0){
			smallest = right;
		}

		if(smallest != i){
			// swap elements
			T old = heap.get(smallest);
			heap.set(smallest, heap.get(i));
			heap.set(i, old);
			bubble_down(smallest);
		}

	}

	private void bubble_up(int i){
		if(i == 0){ return; }

		int parent = this.parent(i);

		// if this is smaller than parent
		if(c.compare(heap.get(i), heap.get(parent)) < 0){
			// swap elements
			T old = heap.get(parent);
			heap.set(parent, heap.get(i));
			heap.set(i, old);
			bubble_up(parent);
		}

	}

	public String toString(){
		if(heap.size() == 0){ return "Heap Empty"; }
		String result = "";
		int next_level = 1;
		for(int i = 0; i < heap.size(); i++){
			if(i >= 2 * next_level - 1){
				result += "\n";
				next_level = next_level * 2;
			}
			result += heap.get(i) + " | ";
		}
		return result;
	}

	public static void test(){
		System.out.println("test");
		PriorityHeap.IntegerComparator ci = new PriorityHeap.IntegerComparator();
		PriorityHeap<Integer> p = new PriorityHeap<Integer>(ci);
		for(int i = 10; i > 0; i--){
			p.heap.add(new Integer(i));
		}
		System.out.println("init heap\n"+p);
		p.heapfy();
		System.out.println("heapy result\n"+p);
		p.add(new Integer(0));
		System.out.println("add 0 result\n"+p);
		p.remove();
		System.out.println("remove result\n"+p);
	}
	
	// return -1, if o1 < o2
	// return  1, if o1 > o2
	// return  0, if o1 = o2
	public static class IntegerComparator implements Comparator<Integer>{
		public int compare(Integer o1, Integer o2){
			if(o1.intValue() > o2.intValue()){
				return 1;
			}else if(o1.intValue() < o2.intValue()){
				return -1;
			}else{
				return 0;
			}
		}
	}
	
	// return -1, if o1 < o2
	// return  1, if o1 > o2
	// return  0, if o1 = o2
	public static class TreeNodeComparator implements Comparator<TreeNode>{
		public int compare(TreeNode o1, TreeNode o2){
			if(o1.h > o2.h){
				return 1;
			}else if(o1.h < o2.h){
				return -1;
			}else{
				return 0;
			}
		}
	}







}