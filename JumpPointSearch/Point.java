package JumpPointSearch;

public class Point{
	public static final Point zero = new Point(0, 0);
	private int x, y;

	public Point(){
		this(0);
	}

	public Point(int x){
		this(x, x);
	}

	public Point(int x, int y){
		this.x = x;
		this.y = y;
	}

	public Point(Point p){
		this.x = p.x;
		this.y = p.y;
	}

	public int getX(){ return x; }
	public int getY(){ return y; }

	public static double distance(Point p1, Point p2){
		return Math.sqrt(Math.pow((p1.x-p2.x),2) + Math.pow((p1.y-p2.y),2));
	}

	public Point add(Point p){
		return new Point(this.x + p.x, this.y + p.y);
	}

	public Point add(int x, int y){
		return new Point(this.x + x, this.y + y);
	}

	public boolean equals(Point p){
		return p.x == this.x && p.y == this.y;
	}

	public String toString(){
		return "["+x+","+y+"]";
	}
}