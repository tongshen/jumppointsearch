JAR = JPS.jar
#SOURCE  = JumpPointSearch/JumpPointMain.java JumpPointSearch/ImageMap.java
SOURCE = JumpPointSearch/*.java
#BUILDDIR = ../build
OBJECT = $(SOURCE:.java=.class)

COMPILER  = javac

$(OBJECT) : $(SOURCE)
	$(COMPILER) -d ./ $(SOURCE)

run : $(OBJECT)
	java JumpPointSearch.JumpPointMain f ./JumpPointSearch/test_hard_1.png 10


$(JAR) : $(OBJECT)
	jar cvfm $(JAR) Manifest.txt JumpPointSearch/*.class

jar : $(JAR)

runjar : $(JAR)
	java -jar $(JAR)

clean : 
	rm $(OBJECT)