Jump Point Search Algorithm Implementation

To compile use this command-->

javac -d ./ JumpPointSearch/*.java

cd into the folder where the makefile is located and use this command to run-->

java JumpPointSearch.JumpPointMain f ./JumpPointSearch/test_hard_1.png 10




The color for the goal is 0XFFF44336(red), The color for start is 0XFF8BC34A(green)

a means animation mode, you can also change it to f to run without stopping.

10 means the zoom level of the result image, increase it to make the image bigger

There are some default image, try ->test_simple_1.png<- 
Or ->test_median_1.png<- Or ->test_hard_1.png<-


Reference:

https://en.wikipedia.org/wiki/Jump_point_search

https://zerowidth.com/2013/05/05/jump-point-search-explained.html

http://users.cecs.anu.edu.au/~dharabor/data/papers/harabor-grastien-aaai11.pdf
